# EFREI-Network-2-2023

Ici vous trouverez tous les supports de cours concernant le cours Virtualisation Réseau EFREI de l'année 2023-2024.

## [Cours](./cours/README.md)

- [Adresses IP](./cours/ip/README.md)
- [ARP](./cours/arp/README.md)
- [DHCP](./cours/dhcp/README.md)
- [Routing](./cours/routing/README.md)
- [NAT](./cours/nat/README..md)
- [TCP et UDP](./cours/tcp_udp/README.md)

## [TP](./tp/README.md)

- [TP1 : Remise dans le bain](./tp/1/README.md)
- [TP2 : Routage, DHCP et DNS](./tp/2/README.md)
- [TP3 : Vers un réseau d'entreprise](./tp/3/README.md)
- [TP4 : ARP Poisoning](./tp/4/README.md)

## [Memo](./memo/README.md)

- [Installation de VM Rocky Linux](./memo/install_vm.md)
- [Manipulations Réseau sous Rocky Linux](./memo/rocky_network.md)
