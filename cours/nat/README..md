# NAT

> Prérequis : bien appréhender la notion de [routage](../routing/README.md).

- [NAT](#nat)
  - [1. IP privées/IP publiques](#1-ip-privéesip-publiques)
  - [2. Why NAT ?](#2-why-nat-)
  - [3. NAT à la rescousse](#3-nat-à-la-rescousse)

Le ***NAT*** est un cas particulier du routage. Le *NAT* permet à un routeur de router des paquets qui passent d'un domaine privé (adresse locale et privée dans un *LAN*) à un domaine public (un réseau étendu ou *WAN*, comme Internet).

## 1. IP privées/IP publiques

Avant de rentrer dans le détail du NAT, il est nécessaire de distinguer les adresses IP publiques des adresses privées.

Une IP est dite *privée* si elle appartient à l'une des plages suivantes :

- `192.168.0.0/16`
  - par exemple, le réseau `192.168.34.0/24` est privé
- `10.0.0.0/8`
  - par exemple, le réseau `10.33.0.0/22` est privé
- `172.16.0.0/12`
  - par exemple, le réseau `172.16.37.0/24` est privé

Cette convention fait partie du protocole IP lui-même.  
**Ces IPs "privées" peuvent être utilisées par n'importe qui de façon arbitraire pour attribuer des IPs à des machines dans des réseaux locaux (ou *LAN*).**

> Il existe d'autres plages IPs que nous allons ignorer pour le cours, pour simplifier le propos. Vous pourrez trouver [une liste exhaustive des plages d'adresses IP réservées sur Wikipedia par exemple](https://en.wikipedia.org/wiki/Reserved_IP_addresses).

---

Les IPs n'appartenant à aucune plage réservée sont dites "publiques". Les IPs publiques permettent d'avoir une adresse sur Internet, un réseau global, qui permet de relier tous les autres réseaux entre eux.  

Ce sont des organismes comme le RIPE qui gère l'attribution des adresses IP publiques. [Toutes les infos ici](https://www.iana.org) pour les curieux.

**Il est impossible, en utilisant une IP publique, de joindre une IP privée directement.**

> Ce genre de normes arbitraires (la séparation des adresses privées et publiques en l'occurrence), c'est défini dans **des documents qu'on appelle les RFCs**. La [RFC 1918](https://datatracker.ietf.org/doc/html/rfc1918) est celle qui propose ce concept d'IPs privées et publiques. Les RFCs sont des documents que n'importe qui peut choisir d'écrire (il y a même des [RFCs troll](https://www.rfc-editor.org/rfc/rfc2549)) et que tout le monde peut choisir d'ignorer. **Ou pas : tous les protocoles dont vous avez connaissance (HTTP, DNS, DHCP, NAT etc.) ont été définis dans des RFCs.** Toutes les RFCs sont évidemment en libre accès en ligne.

## 2. Why NAT ?

Lorsqu'un paquet transite sur le réseau, il contient l'adresse IP de la machine de destination et celle de la machine source.

Dans le cas très courant où le client d'un réseau local veut joindre une machine sur Internet, le routage simple ne suffit plus.

Prenons le cas suivant :

```schema
    LAN1 : 192.168.1.0/24             WAN (internet)
+---------------------------+ +---------------------------+
|                                        |                           |                   |
| u------------------------------------- | u------------------------ | u---------------- |
|                                        |                           | 98.24.14.144      |
|                                        |                           | C           +---+ |
| D        .254+-+-+-+<--------------+ S |                           |                   |
| +----------------->+  R                | +-+-+                     |                   |
|                                        |                           |                   |                 ^
|                                        | +-+---+-----------------+ |                   |
|                                        | .13                       |                   | |87.238.12.14   B
| +---+                                  |                           |                   |
|                                        | PC1+<--------------+      |                   |
| +---+        A                         |                           |                   |
|                                        |                           |                   |
|                                        |                           |                   |
|                                        |                           |                   |
+---------------------------+ +---------------------------+
```

Les lettres `A`, `B`, `C` et `D` représente les différents moments dans la vie du paquet :

- `A` : aller du paquet : du PC1 au routeur
- `B` : aller du paquet : du routeur au serveur
- `C` : retour du paquet : du serveur au routeur
- `D` : retour du paquet : du routeur au PC1

Réseaux :

| Nom du réseau | Adresse du réseau  |
| ------------- | ------------------ |
| LAN1          | `10.1.1.0/24`      |
| Internet      | Adresses publiques |

Machines :

| Nom     | Adresse LAN1 | IP publique    |
| ------- | ------------ | -------------- |
| Routeur | `10.1.1.254` | `87.238.12.14` |
| PC1     | `10.1.1.13`  | x              |
| Serveur | x            | `98.24.14.144` |

---

Examinons le paquet qui transite sur le réseau, à chaque étape de son transit :

| Moment | Adresse source | Adresse destination |
| ------ | -------------- | ------------------- |
| `A`    | `10.1.1.13`    | `98.24.14.144`      |
| `B`    | `10.1.1.13`    | `98.24.14.144`      |
| `C`    | `98.24.14.144` | `10.1.1.13`         |
| `D`    | `98.24.14.144` | `10.1.1.13`         |

Si on examine bien le tableau, à partir de `C`, le serveur renvoie le paquet. Il souhaite renvoyer le paquet vers `10.1.1.13`. **Or, c'est une adresse privée**, il ne peut donc pas joindre cette IP en utilisant son IP publique.

**En réalité, le paquet ne repartira jamais du serveur, et PC1 ne recevra jamais sa réponse. Les étapes `C` et `D` n'existent pas.**

## 3. NAT à la rescousse

Le *NAT* permet de résoudre ce problème. Le concept du *NAT* est de changer les adresses dans le paquet, à la volée.

Le *NAT* pour *Network Address Translation* est une fonctionnalité des routeurs. En utilisant le *NAT* sur un routeur, on lui informe qu'il doit changer les adresses IP des paquets qui le traversent.

En reprenant l'exemple du dessus, mais en demandant au routeur de faire du *NAT*, le tableau des différentes étapes de vie serait le suivant :

| Moment | Adresse source | Adresse destination |
| ------ | -------------- | ------------------- |
| `A`    | `10.1.1.13`    | `98.24.14.144`      |
| `B`    | `87.238.12.14` | `98.24.14.144`      |
| `C`    | `98.24.14.144` | `87.238.12.14`      |
| `D`    | `98.24.14.144` | `10.1.1.13`         |

**Le routeur change l'IP source du paquet qui sort.** Ca se passe avant l'étape `B`.  
**Puis change de nouveau l'IP de destination du paquet de réponse.** Ca se passe après l'étape `C`.

Ceci a pour effet :

- le serveur peut correctement répondre à l'IP publique (étape `C`)
- le client ne voit aucune différence : le paquet qui lui revient a une adresse source et une adresse destination qui sont cohérentes avec ce qu'il a envoyé (voir les étapes `A` et `D`)

➜ **Donc un routeur normal, il change les adresses MAC des trames qui le traversent pour effectuer l'opération de routage. Et s'il fait du NAT en plus, il change aussi les adresses IP des paquets IP.**

![NAT](./img/nat.jpg)