# TP3 : Vers un réseau d'entreprise

Petit à petit, au fil des cours et TPs on appréhende de nouveaux protocoles et de nouvelles techniques très répandus dans le milieu pro.

Ainsi, ce 3ème TP a pour optique de travailler :

- les protocoles et techniques vus dans les cours précédents
- une gestion de plusieurs réseaux (routage)
- un serveur DNS actif
- la fourniture d'un service d'entreprise (ce sera un bête site web dans notre cas)

![Prove it's not the network](./img/not_the_network.jpg)

## Prérequis

> *Aucune des manipulations réalisées dans cette section Prérequis ne doit figurer dans le compte-rendu.*

- GNS3 configuré et fonctionnel
- VirtualBox fonctionnel, avec une VM prête à être clonée
- comme d'hab, munissez-vous du [**mémo**](../../../memo/rocky_network.md) Rocky au sujet du réseau, tout est dedans

**Pour chaque VM du TP :**

- [ ] aucune carte NAT ne doit être utilisée
- [ ] vous devez administrer les machines avec une connexion SSH
- [ ] les machines doivent avoir un nom et une adresse IP tels que demandés dans le sujet

> Le non respect de ces consignes ça fait que je deviens **tout rouge**.

## Suite du TP

Pour aérer un peu le tout, j'ai découpé le TP en deux sections.

- [d'abord on **prépare la topo GNS3**, et on met en place le routage](./network/README.md)
- [ensuite on met en place des **services réseau** au sein de cette topologie](./network_services/README.md)
