# I. DHCP

Bon, vous devez être rodés nan maintenant ?!

🌞 **Setup de la machine `dhcp.net1.tp3`**

- référez-vous au TP1 pour l'installation et la config du serveur DHCP
- il doit donner des IPs aux clients du réseau 1 entre `10.3.1.50/24` et `10.3.1.99/24`
- il doit aussi informer les clients du réseau 1 de l'adresse de leur passerelle
- il doit aussi indiquer l'adresse IP `1.1.1.1` comme serveur DNS utilisable par les clients
- je veux voir le fichier de config dans le compte-rendu

🌞 **Preuve !**

- effectuer une demande d'adresse IP depuis `node1.net1.tp3`
- montrez l'IP obtenue
- montrez que votre table de routage a été mise à jour
- montrez l'adresse du serveur DNS que vous utilisez
- prouvez que vous avez un accès internet normal avec un `ping`
